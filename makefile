#Copyright 2018 Rexy712 of Rexy & Co
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

#Makefile for project rex-edid

#Project directory and file setup
SOURCE_DIRS:=src
OBJDIR:=obj
DEPDIR:=obj/dep
INCLUDE_DIRS:=include
EXT:=c
MAIN_EXECUTABLE:=rex-edid

#Compiler/Linker setup
CC:=gcc
CFLAGS:=-x c -g -std=c11 -Wall -pedantic -Wextra
release: CFLAGS+=-O2
LDFLAGS=
LDLIBS:=-lxcb -lxcb-randr
STRIP:=strip

mkdir=mkdir -p $(1)
rm=rm -f $(1)
rmdir=rm -rf $(1)
move=mv $(1) $(2)

#Internal management of sources/objects and additional cflags
INTERNAL_CFLAGS=-c $(foreach dir,$(INCLUDE_DIRS),-I"$(dir)") -MMD -MP -MF"$(DEPDIR)/$(notdir $(subst .o,.d,$@))"
SOURCES:=$(foreach source,$(SOURCE_DIRS),$(foreach ext,$(EXT),$(wildcard $(source)/*.$(ext))))
OBJECTS:=$(addprefix $(OBJDIR)/,$(subst \,.,$(subst /,.,$(addsuffix .o,$(SOURCES)))))

all: $(MAIN_EXECUTABLE)

$(MAIN_EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $^ -o "$(basename $@)" $(LDLIBS)

.PHONY: release
release: $(OBJECTS)
	$(CC) $(LDFLAGS) $^ -o "$(basename $(MAIN_EXECUTABLE))" $(LDLIBS)
	$(STRIP) --strip-all "$(MAIN_EXECUTABLE)"

define GENERATE_OBJECTS

$$(OBJDIR)/$(subst \,.,$(subst /,.,$(1))).%.o: $(1)/%
	$$(CC) $$(CFLAGS) $$(INTERNAL_CFLAGS) "$$<" -o "$$@"

endef

$(foreach dir,$(SOURCE_DIRS),$(eval $(call GENERATE_OBJECTS,$(dir))))
$(OBJECTS): | $(OBJDIR) $(DEPDIR)

$(OBJDIR):
	$(call mkdir,"$@")
$(DEPDIR):
	$(call mkdir,"$@")

.PHONY: install
install: $(MAIN_EXECUTABLE)
	$(call mkdir,"${DESTDIR}/usr/bin")
	install -m 755 "$(MAIN_EXECUTABLE)" ${DESTDIR}/usr/bin/

.PHONY: clean
clean:
	$(call rmdir,"$(DEPDIR)")
	$(call rmdir,"$(OBJDIR)")
	$(call rm,"$(MAIN_EXECUTABLE)")

-include $(wildcard $(DEPDIR)/*.d)

